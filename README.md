# coding-interview-challenge

### We are looking for developers

Do you think you have what it takes to join our team? This is the right place to prove it to us, by showing off your amazing coding skills and good software pratices.

Just select one of the two challenges below (or both, if you are feeling brave enough) and we will evaluate you based on the quality of the submitted answer.

Each challenge should take you around 4 hours to solve, and we only accept answers that use C++ or Java.


### How to submit

1. Select one of the challenges
2. Create a private fork of this project, with reading permissions to the user Codavel
3. Add your magic stuff
4. Submit a Merge Request to the user Codavel
5. Wait for our feedback


### What will be evaluated

* If your code is readable and well structured
* The selected solution
* Your commits, both in terms of quality and quantity
* Oh! And if your code works :) 


### The problems

* [`Client/Server communication through UDP`](challenges/UDP.md)
* [`Cache`](challenges/Cache.md)
